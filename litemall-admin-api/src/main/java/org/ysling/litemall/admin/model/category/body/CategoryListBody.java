package org.ysling.litemall.admin.model.category.body;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.ysling.litemall.db.entity.PageBody;

/**
 * @author Ysling
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CategoryListBody extends PageBody {

}
