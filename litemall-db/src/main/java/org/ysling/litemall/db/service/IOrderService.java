package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallOrder;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IOrderService extends IBaseService<LitemallOrder> {

}
