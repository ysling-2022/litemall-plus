package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallAdmin;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 管理员表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IAdminService extends IBaseService<LitemallAdmin> {

}
