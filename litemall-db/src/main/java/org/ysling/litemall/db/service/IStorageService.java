package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallStorage;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 文件存储表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IStorageService extends IBaseService<LitemallStorage> {

}
