package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallCollect;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 收藏表 服务类
 * </p>
 *
 * @author ysling
 */
public interface ICollectService extends IBaseService<LitemallCollect> {

}
