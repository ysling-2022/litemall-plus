package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallFootprint;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 用户浏览足迹表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IFootprintService extends IBaseService<LitemallFootprint> {

}
