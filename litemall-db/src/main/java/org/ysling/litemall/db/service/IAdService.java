package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallAd;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 广告表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IAdService extends IBaseService<LitemallAd> {

}
