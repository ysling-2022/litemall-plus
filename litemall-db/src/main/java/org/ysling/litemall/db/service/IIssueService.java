package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallIssue;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 常见问题表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IIssueService extends IBaseService<LitemallIssue> {

}
