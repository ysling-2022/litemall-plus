package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallFeedback;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 意见反馈表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IFeedbackService extends IBaseService<LitemallFeedback> {

}
