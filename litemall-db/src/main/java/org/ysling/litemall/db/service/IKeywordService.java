package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallKeyword;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 关键字表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IKeywordService extends IBaseService<LitemallKeyword> {

}
