package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallCart;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 购物车商品表 服务类
 * </p>
 *
 * @author ysling
 */
public interface ICartService extends IBaseService<LitemallCart> {

}
