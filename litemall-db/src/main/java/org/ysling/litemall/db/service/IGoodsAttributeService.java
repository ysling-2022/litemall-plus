package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallGoodsAttribute;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 商品参数表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IGoodsAttributeService extends IBaseService<LitemallGoodsAttribute> {

}
