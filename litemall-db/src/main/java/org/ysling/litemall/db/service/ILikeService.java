package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallLike;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 点赞表 服务类
 * </p>
 *
 * @author ysling
 */
public interface ILikeService extends IBaseService<LitemallLike> {

}
