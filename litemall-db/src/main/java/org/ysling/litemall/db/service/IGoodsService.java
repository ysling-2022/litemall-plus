package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallGoods;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 商品基本信息表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IGoodsService extends IBaseService<LitemallGoods> {

}
