package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallPermission;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IPermissionService extends IBaseService<LitemallPermission> {

}
